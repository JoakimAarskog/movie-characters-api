# Movie Characters API

This project is an API application in C#. 

# Table of contents

  - [Background](#background)
  - [Requirements Appendix A](#requirements-appendix-a)
  - [Requirements Appendix B](#requirements-appendix-b)
  - [Install](#install)
  - [Usage](#usage)
  - [Author](#author)
  - [License](#license)

## Background

This project is made as an assignment for the Noroff Accelerate program.

## Requirements Appendix A 
- Create a code first MovieDatabase. 
- Diffrent requirements for Character, Movie and Franchise tabel must be forfilled.
- Create dummy data by using seeding. 
  

## Requirements Appendix B

- Create full CRUD for Movies, Characters and Franchises 
- We have alos created People and Genre
- This requires:
    GetAll, 
    GetById,
    Put,
    Post,
    Delete

- It is required to be able to get all Characters in a Movie 
- It is required to get all Movies in a Franchise 
- It is required to get all Characters in a Franchise

- Able to update Characters in a Movie 
- Able to update Movies in Franchise 

- Create DTOs for all model vlasses the client interact with 

- Document the calls with swagger 

## Install

Clone the gitlab repository `git clone https://gitlab.com/JoakimAarskog/movie-characters-api`

## Usage

Open the project in [Visual Studio](https://visualstudio.microsoft.com/) and run the program

## Author
Gitlab [@JoakimAarskog](https://gitlab.com/JoakimAarskog)  
GitLab [@ThomasHD](https://gitlab.com/ThomasHD)

## License

MIT

