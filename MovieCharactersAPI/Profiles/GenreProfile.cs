﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Genre;

namespace MovieCharactersAPI.Profiles
{
    public class GenreProfile : Profile
    {
        public GenreProfile()
        {
            CreateMap<Genre, GenreGetDTO>();
            CreateMap<GenrePostDTO, Genre>();
            CreateMap<GenrePutDTO, Genre>();
        }
    }
}
