﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Franchises;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseGetDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies!.Select(s => s.Id).ToList()));

            CreateMap<FranchisePostDTO, Franchise>();
            CreateMap<FranchisePutDTO, Franchise>();
            
        }
    }
}
