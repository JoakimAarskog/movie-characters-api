﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Characters;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterGetDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies!.Select(s => s.Id).ToList()));

            CreateMap<CharacterPostDTO, Character>();
            CreateMap<CharacterPutDTO, Character>();
        }
    }
}
