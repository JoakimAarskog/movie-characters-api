﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Director;

namespace MovieCharactersAPI.Profiles
{
    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<Person, PersonGetDTO>();
            CreateMap<PersonPostDTO, Person>();
            CreateMap<PersonPutDTO, Person>();
        }
    }
}
