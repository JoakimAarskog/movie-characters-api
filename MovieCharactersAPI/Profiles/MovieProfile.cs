﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Movies;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieGetDTO>()
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(c => c.Characters!.Select(s => s.Id).ToList()));

            CreateMap<MoviePostDTO, Movie>();
            CreateMap<MoviePutDTO, Movie>();
        }
    }
}
