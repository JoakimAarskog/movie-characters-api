﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieCharactersAPI.Models;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    partial class MovieDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.HasKey("CharacterId", "MovieId");

                    b.HasIndex("MovieId");

                    b.ToTable("CharacterMovie");

                    b.HasData(
                        new
                        {
                            CharacterId = 1,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 1,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 2,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 2,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 3,
                            MovieId = 3
                        },
                        new
                        {
                            CharacterId = 4,
                            MovieId = 3
                        },
                        new
                        {
                            CharacterId = 5,
                            MovieId = 3
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Alias")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.Property<int>("Gender")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "Tim the timable",
                            Gender = 0,
                            Name = "Tim",
                            Picture = "bilde.url"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Tem the timable",
                            Gender = 1,
                            Name = "Tem",
                            Picture = "bilde.url"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "Nommy",
                            Gender = 1,
                            Name = "Nom",
                            Picture = "bilde.url"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "Tommy",
                            Gender = 0,
                            Name = "Tom",
                            Picture = "bilde.url"
                        },
                        new
                        {
                            Id = 5,
                            Alias = "Monny",
                            Gender = 0,
                            Name = "Mon",
                            Picture = "bilde.url"
                        },
                        new
                        {
                            Id = 6,
                            Alias = "Rommy",
                            Gender = 0,
                            Name = "Rom",
                            Picture = "bilde.url"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(160)
                        .HasColumnType("nvarchar(160)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.HasKey("Id");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Tim drar på eventyr",
                            Name = "Tim sine eventyr"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Steiner er gøy",
                            Name = "Steiner"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Liker ikke film",
                            Name = "Ingen"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Genre", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Genres");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Ablegøyer"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Morro"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Fun stuff"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int?>("DirectorId")
                        .HasColumnType("int");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<int?>("GenreId")
                        .HasColumnType("int");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ReleaseYear")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("nvarchar(4)");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Trailer")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("DirectorId");

                    b.HasIndex("FranchiseId");

                    b.HasIndex("GenreId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DirectorId = 1,
                            FranchiseId = 1,
                            GenreId = 2,
                            Picture = "Bilde.com",
                            ReleaseYear = "2002",
                            Title = "Tim til nye høyder",
                            Trailer = "youtube.com"
                        },
                        new
                        {
                            Id = 2,
                            DirectorId = 2,
                            FranchiseId = 1,
                            GenreId = 1,
                            Picture = "Bilde.com",
                            ReleaseYear = "2003",
                            Title = "Tim til nye høyder2",
                            Trailer = "youtube.com"
                        },
                        new
                        {
                            Id = 3,
                            DirectorId = 3,
                            FranchiseId = 2,
                            GenreId = 1,
                            Picture = "Bilde.com",
                            ReleaseYear = "2003",
                            Title = "Baluba",
                            Trailer = "youtube.com"
                        },
                        new
                        {
                            Id = 4,
                            DirectorId = 3,
                            GenreId = 2,
                            Picture = "Bilde.com",
                            ReleaseYear = "2003",
                            Title = "Tricky",
                            Trailer = "youtube.com"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Person", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<int>("Gender")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(40)
                        .HasColumnType("nvarchar(40)");

                    b.HasKey("Id");

                    b.ToTable("People");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Age = 54,
                            Gender = 0,
                            Name = "Ron"
                        },
                        new
                        {
                            Id = 2,
                            Age = 43,
                            Gender = 1,
                            Name = "Nina"
                        },
                        new
                        {
                            Id = 3,
                            Age = 35,
                            Gender = 0,
                            Name = "Kåre"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Models.Character", null)
                        .WithMany()
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieCharactersAPI.Models.Movie", null)
                        .WithMany()
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Movie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Models.Person", "Director")
                        .WithMany()
                        .HasForeignKey("DirectorId");

                    b.HasOne("MovieCharactersAPI.Models.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId");

                    b.HasOne("MovieCharactersAPI.Models.Genre", "Genre")
                        .WithMany()
                        .HasForeignKey("GenreId");

                    b.Navigation("Director");

                    b.Navigation("Franchise");

                    b.Navigation("Genre");
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
