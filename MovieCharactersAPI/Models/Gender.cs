﻿namespace MovieCharactersAPI.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
