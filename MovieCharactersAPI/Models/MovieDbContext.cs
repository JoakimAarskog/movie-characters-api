﻿using Microsoft.EntityFrameworkCore;

namespace MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public virtual DbSet<Character> Characters { get; set; } = null!;
        public virtual DbSet<Movie> Movies { get; set; } = null!;
        public virtual DbSet<Franchise> Franchises { get; set; } = null!;
        public virtual DbSet<Person> People { get; set; } = null!;
        public virtual DbSet<Genre> Genres { get; set; } = null!;

        protected MovieDbContext()
        {
        }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //seed data

            // Genre 
            modelBuilder.Entity<Genre>().HasData(new Genre { Id = 1, Name = "Ablegøyer" });
            modelBuilder.Entity<Genre>().HasData(new Genre { Id = 2, Name = "Morro" });
            modelBuilder.Entity<Genre>().HasData(new Genre { Id = 3, Name = "Fun stuff" });

            // Person
            modelBuilder.Entity<Person>().HasData(new Person { Id = 1, Name = "Ron", Age = 54, Gender = Gender.Male });
            modelBuilder.Entity<Person>().HasData(new Person { Id = 2, Name = "Nina", Age = 43, Gender = Gender.Female });
            modelBuilder.Entity<Person>().HasData(new Person { Id = 3, Name = "Kåre", Age = 35, Gender = Gender.Male });

            // Characters
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, Name = "Tim", Alias = "Tim the timable", Picture = "bilde.url", Gender = Gender.Male });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, Name = "Tem", Alias = "Tem the timable", Picture = "bilde.url", Gender = Gender.Female });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, Name = "Nom", Alias = "Nommy", Picture = "bilde.url", Gender = Gender.Female });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, Name = "Tom", Alias = "Tommy", Picture = "bilde.url", Gender = Gender.Male });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, Name = "Mon", Alias = "Monny", Picture = "bilde.url", Gender = Gender.Male });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 6, Name = "Rom", Alias = "Rommy", Picture = "bilde.url", Gender = Gender.Male });

            // Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "Tim sine eventyr",
                Description = "Tim drar på eventyr"
            });

            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Steiner",
                Description = "Steiner er gøy"
            });

            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 3,
                Name = "Ingen",
                Description = "Liker ikke film"
            });

            // Movies
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                Title = "Tim til nye høyder",
                DirectorId = 1,
                Trailer = "youtube.com",
                GenreId = 2,
                Picture = "Bilde.com",
                ReleaseYear = "2002",
                FranchiseId = 1,
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "Tim til nye høyder2",
                DirectorId = 2,
                Trailer = "youtube.com",
                GenreId = 1,
                Picture = "Bilde.com",
                ReleaseYear = "2003",
                FranchiseId = 1,
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "Baluba",
                DirectorId = 3,
                Trailer = "youtube.com",
                GenreId = 1,
                Picture = "Bilde.com",
                ReleaseYear = "2003",
                FranchiseId = 2,
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 4,
                Title = "Tricky",
                DirectorId = 3,
                Trailer = "youtube.com",
                GenreId = 2,
                Picture = "Bilde.com",
                ReleaseYear = "2003",
            });

            //seeding 
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 3, MovieId = 3 },
                            new { CharacterId = 4, MovieId = 3 },
                            new { CharacterId = 5, MovieId = 3 }
                        );
                    });
        }
    }
}
