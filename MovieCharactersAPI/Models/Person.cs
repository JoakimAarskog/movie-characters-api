﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Person
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(40)]
        public string Name { get; set; } = null!;
        public int Age { get; set; }
        public Gender Gender { get; set; }
    }
}
