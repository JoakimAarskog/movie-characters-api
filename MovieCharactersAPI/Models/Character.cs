﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Character
    {
        //PK
        [Key]
        public int Id { get; set; }

        //Fields
        [MinLength(3)]
        [MaxLength(30)]
        public string Name { get; set; } = null!;

        [MinLength(3)]
        [MaxLength(30)]
        public string Alias { get; set; } = null!;
        public Gender Gender { get; set; }
        public string Picture { get; set; } = null!;

        // Relationship
        public ICollection<Movie>? Movies { get; set; }
    }
}
