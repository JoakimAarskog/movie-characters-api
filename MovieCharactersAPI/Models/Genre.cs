﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Genre
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Name { get; set; } = null!;
    }
}
