﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models
{
    [Table("Franchise")]

    public class Franchise
    {
        //PK
        [Key]
        public int Id { get; set; }

        // Fields
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string Name { get; set; } = null!;

        [Required]
        [MinLength(10)]
        [MaxLength(160)]
        public string Description { get; set; } = null!;

        //Realations 
        public ICollection<Movie>? Movies { get; set; }

    }
}
