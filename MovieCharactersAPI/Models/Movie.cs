﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models
{
    [Table("Movie")]
    public class Movie
    {
        //PK
        [Key]
        public int Id { get; set; }

        // Fields
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Title { get; set; } = null!;

        [Required]
        [MinLength(4)]
        [MaxLength(4)]
        public string ReleaseYear { get; set; } = null!;

        [Required]
        public string Picture { get; set; } = null!;

        [Required]
        public string Trailer { get; set; } = null!;

        // Relationships
        public ICollection<Character>? Characters { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; } = null!;

        public int? DirectorId { get; set; }
        public Person? Director { get; set; } = null!;

        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }

    }
}
