﻿namespace MovieCharactersAPI.Models.DTOs.Genre
{
    public class GenrePutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
