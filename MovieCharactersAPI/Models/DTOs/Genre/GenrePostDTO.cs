﻿namespace MovieCharactersAPI.Models.DTOs.Genre
{
    public class GenrePostDTO
    {
        public string Name { get; set; } = null!;
    }
}
