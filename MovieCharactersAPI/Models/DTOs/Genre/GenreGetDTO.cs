﻿namespace MovieCharactersAPI.Models.DTOs.Genre
{
    public class GenreGetDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
