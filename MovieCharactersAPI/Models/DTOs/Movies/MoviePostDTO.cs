﻿using MovieCharactersAPI.Models.DTOs.Director;

namespace MovieCharactersAPI.Models.DTOs.Movies
{
    public class MoviePostDTO
    {
        public string Title { get; set; } = null!;
        public int Genre { get; set; }
        public string ReleaseYear { get; set; } = null!;
        public int Director { get; set; }
        public string Picture { get; set; } = null!;
        public string Trailer { get; set; } = null!;
    }
}
