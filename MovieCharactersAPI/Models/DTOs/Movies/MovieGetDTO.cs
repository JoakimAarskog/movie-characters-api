﻿namespace MovieCharactersAPI.Models.DTOs.Movies
{
    public class MovieGetDTO
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public int GenreId { get; set; }
        public string ReleaseYear { get; set; } = null!;
        public int DirectorId { get; set; }
        public string Picture { get; set; } = null!;
        public string Trailer { get; set; } = null!;
        public int FranchiseId { get; set; }

        public List<int> Characters { get; set; } = null!;
    }
}
