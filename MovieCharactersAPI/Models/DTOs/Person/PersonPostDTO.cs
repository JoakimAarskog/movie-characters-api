﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTOs.Director
{
    public class PersonPostDTO
    {
        public string Name { get; set; } = null!;
        public int Age { get; set; }
        public string Gender { get; set; } = null!;
    }
}
