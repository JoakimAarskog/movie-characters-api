﻿namespace MovieCharactersAPI.Utils.Exceptions
{
    internal class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string? message) : base(message)
        {
        }

    }
}