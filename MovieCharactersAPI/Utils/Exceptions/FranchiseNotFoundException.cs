﻿namespace MovieCharactersAPI.Utils.Exceptions
{
    [Serializable]
    internal class FranchiseNotFoundException : EntityNotFoundException
    {
        public FranchiseNotFoundException() : base("Franchise with not found with that Id.")
        {
        }
    }
}