﻿namespace MovieCharactersAPI.Utils.Exceptions
{
    [Serializable]
    internal class MovieNotFoundException : EntityNotFoundException
    {
        public MovieNotFoundException() : base("Movie not found with that Id.")
        {
        }
    }
}