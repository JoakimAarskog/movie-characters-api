﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Utils.Exceptions
{
    internal class CharacterNotFoundException : EntityNotFoundException
    {
        public CharacterNotFoundException() : base("Character not found with that Id.")
        {
        }

    }
}