﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Characters;
using MovieCharactersAPI.Models.DTOs.Franchises;
using MovieCharactersAPI.Models.DTOs.Movies;
using MovieCharactersAPI.Services.Franchises;
using MovieCharactersAPI.Utils.Exceptions;
using System.Net;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// Controller for Franchises
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        /// <summary>
        /// Constructor for FranchiseController, takes a mapper and a service as paramaters.
        /// </summary>
        /// <param name="mapper">Autmapper for DTOs</param>
        /// <param name="franchiseService">Instance of IFranchiseService</param>
        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>List of FranchiseGetDTO</returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseGetDTO>>> GetFranchises()
        {
            return Ok(_mapper.Map<List<FranchiseGetDTO>>(await _franchiseService.GetAllAsync()));
        }

        /// <summary>
        /// Gets all the characters in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of CharacterGetDTO</returns>
        // GET: api/Franchises
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetFranchiseCharacters(int id)
        {
            try
            {
                return Ok(_mapper.Map<List<CharacterGetDTO>>(await _franchiseService.GetFranchiseCharactersAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets franchise with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A single CharacterGetDTO</returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseGetDTO>> GetFranchise(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseGetDTO>(
                        await _franchiseService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all movies inn a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieGetDTO>>> GetMoviesForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieGetDTO>>(
                            await _franchiseService.GetMoviesByFranchiseIdAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Update franchise with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>Status code NoContent if successfull</returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchisePutDTO franchise)
        {
            if (id != franchise.Id)
                return BadRequest();

            try
            {
                await _franchiseService.UpdateAsync(
                        _mapper.Map<Franchise>(franchise)
                    );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Adds movies to a franchise
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns>Status code NoContent if successfull</returns>
        // PUT: api/Franchises/5/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> PutMoviesFranchiseAsync(int[] movieIds, int id)
        {

            try
            {
                await _franchiseService.UpdateMoviesForFranchiseAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Create a new franchise
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns>FranchisePostDTO</returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostFranchise(FranchisePostDTO franchiseDTO)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Delete a franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Status code NoContent if successfull</returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            try
            {
                await _franchiseService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }
}
