﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Director;
using MovieCharactersAPI.Utils.Exceptions;
using System.Net;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly MovieDbContext _context;

        public PeopleController(IMapper mapper, MovieDbContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/People
        /// <summary>
        /// Gets all people
        /// </summary>
        /// <returns>List of people</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonGetDTO>>> GetPeople()
        {
            return Ok(_mapper.Map<List<PersonGetDTO>>(await _context.People.ToListAsync()));
        }

        // GET: api/People/5
        /// <summary>
        /// Get a person with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>PersonGetDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonGetDTO>> GetPerson(int id)
        {
            try
            {
                return Ok(_mapper.Map<PersonGetDTO>
                    (
                        await _context.People.FindAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        // PUT: api/People/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update a person with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="personDTO"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, PersonPutDTO personDTO)
        {
            if (id != personDTO.Id)
            {
                return BadRequest();
            }

            _context.Entry(_mapper.Map<Person>(personDTO)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/People
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a new person 
        /// </summary>
        /// <param name="personDTO"></param>
        /// <returns>CreatedAtAction</returns>
        [HttpPost]
        public async Task<ActionResult<PersonPostDTO>> PostPerson(PersonPostDTO personDTO)
        {

            Person person = _mapper.Map<Person>(personDTO);
            _context.People.Add(person);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPerson", new { id = person.Id }, person);
        }

        // DELETE: api/People/5
        /// <summary>
        /// Delete person with given ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NoContent</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePerson(int id)
        {
            var person = await _context.People.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }

            _context.Movies.Where(m => m.DirectorId == id).Load();
            _context.People.Remove(person);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PersonExists(int id)
        {
            return _context.People.Any(e => e.Id == id);
        }
    }
}
