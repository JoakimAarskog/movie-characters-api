﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Genre;
using MovieCharactersAPI.Utils.Exceptions;
using System.Net;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly MovieDbContext _context;

        public GenresController(IMapper mapper, MovieDbContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all genres
        /// </summary>
        /// <returns>List of genres</returns>
        // GET: api/Genres
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GenreGetDTO>>> GetGenre()
        {
            return Ok(_mapper.Map<List<GenreGetDTO>>(await _context.Genres.ToListAsync()));
        }

        /// <summary>
        /// Get a genre with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>GenreGetDTO</returns>
        // GET: api/Genres/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GenreGetDTO>> GetGenre(int id)
        {
            try
            {
                return Ok(_mapper.Map<GenreGetDTO>
                    (
                        await _context.Genres.FindAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Update a genre with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="genreDTO"></param>
        /// <returns>NoContent</returns>
        // PUT: api/Genres/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGenre(int id, GenrePutDTO genreDTO)
        {
            if (id != genreDTO.Id)
            {
                return BadRequest();
            }

            _context.Entry(_mapper.Map<Genre>(genreDTO)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GenreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new genre 
        /// </summary>
        /// <param name="genreDTO"></param>
        /// <returns>CreatedAtAction</returns>
        // POST: api/Genres
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GenrePostDTO>> PostGenre(GenrePostDTO genreDTO)
        {
            Genre genre = _mapper.Map<Genre>(genreDTO);
            _context.Genres.Add(genre);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGenre", new { id = genre.Id }, genre);
        }


        /// <summary>
        /// Delete genre with given ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NoContent</returns>
        // DELETE: api/Genres/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGenre(int id)
        {
            var genre = await _context.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            _context.Genres.Remove(genre);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GenreExists(int id)
        {
            return _context.Genres.Any(e => e.Id == id);
        }
    }
}
