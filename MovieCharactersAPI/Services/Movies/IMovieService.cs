﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        Task<ICollection<Character>> GetCharacterByMovieIdAsync(int id);
        Task UpdateCharactersInMovies(int[] characters, int id);
    }
}
