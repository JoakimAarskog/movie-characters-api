﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Utils.Exceptions;

namespace MovieCharactersAPI.Services.Movies
{
    public class MovieServiceimpl : IMovieService
    {
        private readonly MovieDbContext _dbContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        public MovieServiceimpl(MovieDbContext dbContext, ILogger<CharacterServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddAsync(Movie entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var movie = await _dbContext.Movies.Include(m => m.Characters).Include(m => m.Franchise).SingleOrDefaultAsync(f => f.Id == id); ;
            // Log and throw pattern
            if (movie == null)
            {
                _logger.LogError($"Movie not found with Id: {id}");
                throw new MovieNotFoundException();
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _dbContext.Movies.Remove(movie);
            await _dbContext.SaveChangesAsync(); ;
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _dbContext.Movies
             .Include(c => c.Characters)
             .Include(m => m.Genre)
             .Include(m => m.Director)
             .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            if (!await MovieExistsAsync(id))
            {
                _logger.LogError($"Movie not found with Id: {id}");
                throw new MovieNotFoundException();
            }
            // Want to include all related data for movie
            return await _dbContext.Movies
                .Where(m => m.Id == id)
                .Include(c => c.Characters)
                .Include(m => m.Genre)
                .Include(m => m.Director)
                .FirstAsync();
        }

        public async Task<ICollection<Character>> GetCharacterByMovieIdAsync(int id)
        {
            return await _dbContext.Characters
           .Where(m => m.Movies!.Any(f => f.Id == id)).Include(m => m.Movies).ToListAsync();
        }

        public async Task UpdateAsync(Movie entity)
        {
            if (!await MovieExistsAsync(entity.Id))
            {
                _logger.LogError($"Movie not found with Id: {entity.Id}");
                throw new MovieNotFoundException();
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateCharactersInMovies(int[] characterIds, int id)
        {
            if (!await MovieExistsAsync(id))
            {
                _logger.LogError($"Character not found with Id: {id}");
                throw new CharacterNotFoundException();
            }
            // First convert the int[] to List<Movie>

            // TODO: Throw character already in movie error

            List<Character> characters = characterIds
                .ToList()
                .Select(sid => _dbContext.Characters
                .Where(s => s.Id == sid).FirstOrDefault())
                .ToList()!;

            // Get character for Id
            Movie movie = await _dbContext.Movies
                .Where(p => p.Id == id)
                .FirstAsync();

            // Set the characters movies
            movie.Characters = characters;
            _dbContext.Entry(movie).State = EntityState.Modified;

            // Save all the changes
            await _dbContext.SaveChangesAsync();
        }

        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _dbContext.Movies.AnyAsync(e => e.Id == id);
        }
    }
}
