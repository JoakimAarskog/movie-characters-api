﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Utils.Exceptions;

namespace MovieCharactersAPI.Services.Franchises
{
    /// <summary>
    /// Implementation of FranchiseService
    /// </summary>
    public class FranchiseServiceimpl : IFranchiseService
    {
        private readonly MovieDbContext _dbContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        /// <summary>
        /// Contructor for FranchiseServiceimpl.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        /// <param name="logger"></param>
        public FranchiseServiceimpl(MovieDbContext dbContext, ILogger<CharacterServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        /// <summary>
        /// Adds a franchise to the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Task.</returns>
        public async Task AddAsync(Franchise entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Delete a franchise by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        /// <exception cref="FranchiseNotFoundException"></exception>
        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _dbContext.Franchises.Include(f => f.Movies).SingleOrDefaultAsync(f => f.Id == id);
            // Log and throw pattern
            if (franchise == null)
            {
                _logger.LogError($"Franchise not found with Id: {id}");
                throw new FranchiseNotFoundException();
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _dbContext.Franchises.Remove(franchise);
            await _dbContext.SaveChangesAsync(); ;
        }

        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>A list of franchises</returns>
        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _dbContext.Franchises
                          .Include(m => m.Movies)
                          .ToListAsync();
        }

        /// <summary>
        /// Get a single franchise from specified id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A single franchise.</returns>
        /// <exception cref="FranchiseNotFoundException"></exception>
        public async Task<Franchise> GetByIdAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError($"Franchise not found with Id: {id}");
                throw new FranchiseNotFoundException();
            }
            // Want to include all related data for movie
            return await _dbContext.Franchises
                .Where(m => m.Id == id)
                .Include(c => c.Movies)
                .FirstAsync();
        }

        /// <summary>
        /// Get all the characters in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of characters</returns>
        /// <exception cref="FranchiseNotFoundException"></exception>
        public async Task<ICollection<Character>> GetFranchiseCharactersAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError($"Franchise not found with Id: {id}");
                throw new FranchiseNotFoundException();
            }

            // TODO: Throw NoCharactersInFranchiseException

            return await _dbContext.Characters
                .Where(m => m.Movies!.Any(f => f.FranchiseId == id)).Include(m => m.Movies).ToListAsync();
        }

        /// <summary>
        /// Updates the specified franchise
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Task</returns>
        /// <exception cref="FranchiseNotFoundException"></exception>
        public async Task UpdateAsync(Franchise entity)
        {
            if (!await FranchiseExistsAsync(entity.Id))
            {
                _logger.LogError($"Franchise not found with Id: {entity.Id}");
                throw new FranchiseNotFoundException();
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Adds movies to a franchise.
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        /// <exception cref="FranchiseNotFoundException">Gets thrown when the franchise is not found</exception>
        public async Task UpdateMoviesForFranchiseAsync(int[] movieIds, int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError($"Franchise not found with Id: {id}");
                throw new FranchiseNotFoundException();
            }
            // First convert the int[] to List<Movie>

            // TODO: Throw movie already in character error

            List<Movie> movies = movieIds
                .ToList()
                .Select(sid => _dbContext.Movies
                .Where(s => s.Id == sid).FirstOrDefault())
                .ToList()!;

            // Get character for Id
            Franchise franchise = await _dbContext.Franchises
                .Where(p => p.Id == id)
                .FirstAsync();

            // Set the characters movies
            franchise.Movies = movies;
            _dbContext.Entry(franchise).State = EntityState.Modified;

            // Save all the changes
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Get all movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of movies</returns>
        public async Task<IEnumerable<Movie?>> GetMoviesByFranchiseIdAsync(int id)
        {
            return await _dbContext.Movies
           .Where(movie => movie.FranchiseId == id).Include(m => m.Characters)
           .ToListAsync();
        }

        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _dbContext.Franchises.AnyAsync(e => e.Id == id);
        }
    }
}
