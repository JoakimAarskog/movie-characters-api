﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        /// <summary>
        /// Gets characters in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        Task<ICollection<Character>> GetFranchiseCharactersAsync(int id);

        /// <summary>
        /// Adds movies to a franchise
        /// </summary>
        /// <param name="characters"></param>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        Task UpdateMoviesForFranchiseAsync(int[] characters, int id);

        Task<IEnumerable<Movie?>> GetMoviesByFranchiseIdAsync(int id);
    }
}
