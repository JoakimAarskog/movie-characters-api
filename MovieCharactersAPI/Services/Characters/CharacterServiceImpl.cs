﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Utils.Exceptions;

namespace MovieCharactersAPI.Services.Characters
{
    public class CharacterServiceImpl : ICharacterService
    {
        private readonly MovieDbContext _dbContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        public CharacterServiceImpl(MovieDbContext dbContext, ILogger<CharacterServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddAsync(Character entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var character = await _dbContext.Characters.Include(f => f.Movies).SingleOrDefaultAsync(f => f.Id == id);
            // Log and throw pattern
            if (character == null)
            {
                _logger.LogError($"Character not found with Id: {id}");
                throw new CharacterNotFoundException();
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _dbContext.Characters.Remove(character);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _dbContext.Characters
              .Include(m => m.Movies)
              .ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            if (!await CharacterExistsAsync(id))
            {
                _logger.LogError($"Character not found with Id: {id}");
                throw new CharacterNotFoundException();
            }
            // Want to include all related data for characters
            return await _dbContext.Characters
                .Where(c => c.Id == id)
                .Include(m => m.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Character entity)
        {
            if (!await CharacterExistsAsync(entity.Id))
            {
                _logger.LogError($"Character not found with Id: {entity.Id}");
                throw new CharacterNotFoundException();
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesForCharacterAsync(int[] movieIds, int id)
        {
            if (!await CharacterExistsAsync(id))
            {
                _logger.LogError($"Character not found with Id: {id}");
                throw new CharacterNotFoundException();
            }
            // First convert the int[] to List<Movie>

            // TODO: Throw movie already in character error

            List<Movie> movies = movieIds
                .ToList()
                .Select(sid => _dbContext.Movies
                .Where(s => s.Id == sid).FirstOrDefault())
                .ToList()!;

            // Get character for Id
            Character character = await _dbContext.Characters
                .Where(p => p.Id == id)
                .FirstAsync();

            // Set the characters movies
            character.Movies = movies;
            _dbContext.Entry(character).State = EntityState.Modified;

            // Save all the changes
            await _dbContext.SaveChangesAsync();
        }

        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _dbContext.Characters.AnyAsync(e => e.Id == id);
        }
    }
}
