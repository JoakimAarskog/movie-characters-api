﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
        Task UpdateMoviesForCharacterAsync(int[] movies, int id);
    }
}
