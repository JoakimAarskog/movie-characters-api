﻿using MovieCharactersAPI.Utils.Exceptions;

namespace MovieCharactersAPI.Services
{
    public interface ICrudService<T, ID>
    {
        /// <summary>
        /// Get all instances of an entity.
        /// </summary>
        /// <returns>A collection of entites</returns>
        Task<ICollection<T>> GetAllAsync();
        /// <summary>
        /// Get a specific entity by its Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A singular entity</returns>
        /// <exception cref="EntityNotFoundException">Thrown when an enitit is not found.</exception>
        Task<T> GetByIdAsync(ID id);
        /// <summary>
        /// Add a new entity.
        /// </summary>
        /// <param name="entity"></param>
        Task AddAsync(T entity);
        /// <summary>
        /// Updates an existing entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <exception cref="EntityNotFoundException">Thrown when an enitit is not found.</exception>
        Task UpdateAsync(T entity);
        /// <summary>
        /// Deletes an entity by its Id.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="EntityNotFoundException">Thrown when an enitit is not found.</exception>
        Task DeleteByIdAsync(ID id);
    }

}
